package qasnamedpattern

import (
	"fmt"
	crflib "gitlab.com/authapon/qascrflib"
)

type (
	NamedPatternDB map[string]string
)

func NamedPatternIdent(data string) string {
	if data == "" {
		return ""
	}

	namedPattern, err := GetNamedPatternDB()
	if err != nil {
		return ""
	}

	longestPattern := GetLongestPattern(namedPattern)
	dataTag := crflib.GetWordTag(data)
	dataOut := crflib.CrfTrainSentence{}
	dataOut.Sentence = []string{}
	dataOut.Label = []string{}
	index := 0
	apply := false
	fmt.Printf("Start -> %s\n", data)
	for {
		if index >= len(dataTag.Sentence) {
			fmt.Printf("Finnished\n")
			break
		}
	ChkPoint:
		for i := longestPattern; i > 0; i-- {
			for i2 := range namedPattern {
				patternTag := crflib.GetWordTag(i2)
				if len(patternTag.Label) == i {
					fmt.Printf("Evaluated -> %s\n", i2)
					if NamedPatternApply(dataTag, patternTag, index) {
						fmt.Printf("Match -> %s\n", i2)
						dataX := crflib.CrfTrainSentence{}
						dataX.Sentence = dataTag.Sentence[index : index+i]
						dataX.Label = dataTag.Label[index : index+i]
						dataXX := crflib.WordTagString(dataX)
						dataOut.Sentence = append(dataOut.Sentence, dataXX)
						dataOut.Label = append(dataOut.Label, namedPattern[i2])
						index += i
						apply = true
						break ChkPoint
					}
					fmt.Printf("Not match !!!\n")
				}
			}
		}
		if !apply {
			dataOut.Sentence = append(dataOut.Sentence, dataTag.Sentence[index])
			dataOut.Label = append(dataOut.Label, dataTag.Label[index])
			index += 1
			fmt.Printf("**** No Evaluated ****\n")
		} else {
			apply = false
		}
	}

	return crflib.WordTagString(dataOut)
}

func NamedPatternApply(data crflib.CrfTrainSentence, pattern crflib.CrfTrainSentence, index int) bool {
	if len(data.Sentence[index:]) < len(pattern.Sentence) {
		return false
	}

	for i := 0; i < len(pattern.Sentence); i++ {
		if pattern.Sentence[i] == "*" && pattern.Label[i] == data.Label[index+i] {
			continue
		} else if pattern.Sentence[i] == data.Sentence[index+i] && pattern.Label[i] == data.Label[index+i] {
			continue
		} else {
			return false
		}
	}
	return true
}

func GetLongestPattern(nDB NamedPatternDB) int {
	longest := 0
	for k, _ := range nDB {
		words := crflib.GetWordTag(k)
		long := len(words.Label)
		if long > longest {
			longest = long
		}
	}
	return longest
}
