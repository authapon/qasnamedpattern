package qasnamedpattern

import (
	"errors"
	moosql "gitlab.com/authapon/moosqlite"
)

func GetNamedPatternDB() (NamedPatternDB, error) {
	namedPattern := make(NamedPatternDB)
	db, err := moosql.GetSQL()
	if err != nil {
		return namedPattern, errors.New("DB connection fail!")
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `pattern`, `tag` from `namedpattern`;")
	rows, err := st.Query()
	if err != nil {
		return namedPattern, errors.New("DB query fail!")
	}
	defer rows.Close()

	for rows.Next() {
		pattern := ""
		tag := ""
		rows.Scan(&pattern, &tag)
		namedPattern[pattern] = tag
	}
	return namedPattern, nil
}
